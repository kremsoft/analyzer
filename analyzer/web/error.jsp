<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta name="description" content="" />
<meta name="author" content="" />
<title>Ardor Media Factory</title>
<link href="favicon.ico" rel="shortcut icon" type="image/x-icon" />
<link href="resources/css/bootstrap.min.css" rel="stylesheet" />
<link href="resources/css/font-awesome.min.css" rel="stylesheet" />
</head>
<body>
	<div class="container-fluid">
		<div class="jumbotron">
			<h2>Error!</h2>
			<p>Oh My Logs!</p>
		</div>
		<hr />
	</div>
	<script src="resources/js/jquery-1.11.2.min.js"></script>
	<script src="resources/js/bootstrap.min.js"></script>
</body>
</html>