package com.ardor.context;

import java.lang.reflect.Method;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.quartz.SchedulerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ardor.Const;
import com.ardor.persistence.PersistenceManager;
import com.ardor.service.scheduler.SchedulerService;

public class ApplicationListener implements ServletContextListener {

	private static final Logger logger = LoggerFactory.getLogger(ApplicationListener.class);

	@Override
	public void contextInitialized(ServletContextEvent event) {
		try {
			runStartUpServices();
		} catch (SchedulerException e) {
			throw new RuntimeException(e);
		}
	}

	private void runStartUpServices() throws SchedulerException {
		logger.debug("runStartUpServices started");
		SchedulerService schedulerService = new SchedulerService();
		schedulerService.deleteAllJobs();
		schedulerService.reCrawlProcessing();
		logger.debug("System running on live");
		if (!Const.REMOTE) {
			logger.debug("System running on local");
			schedulerService.deleteAllJobs();
			schedulerService.reCrawlProcessing();
		}
	}

	@Override
	public void contextDestroyed(ServletContextEvent event) {
		PersistenceManager.closeEntityManagerFactory();
		SchedulerService schedulerService = new SchedulerService();
		schedulerService.shutDown();
		cleanupThread();
		logger.info("contextDestroyed()");
	}

	private void cleanupThread() {
		try {
			Class<?> clazz = Class.forName("com.mysql.jdbc.AbandonedConnectionCleanupThread");
			Method method = (clazz == null ? null : clazz.getMethod("shutdown"));
			if (method != null) {
				method.invoke(null);
			}
		} catch (Throwable t) {
			logger.error(t.getMessage(), t);
		}
	}
}
