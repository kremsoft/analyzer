package com.ardor.job;

import java.io.Serializable;
import java.util.List;

import javax.persistence.EntityManager;

import org.apache.http.HttpStatus;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ardor.entity.Website;
import com.ardor.persistence.PersistenceManager;
import com.ardor.service.WebsiteService;
import com.ardor.service.scheduler.SchedulerService;
import com.ardor.util.DetectHtml;
import com.ardor.util.DownloadUtil;
import com.ardor.util.HttpResponseUtil;
import com.ardor.util.PageUtil;

public class PersistSiteHierarchyJob implements Serializable, Job {

	private static final long serialVersionUID = 1L;

	private static final Logger logger = LoggerFactory.getLogger(PersistSiteHierarchyJob.class);

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		JobDataMap jobDataMap = context.getJobDetail().getJobDataMap();

		int definedDepth = (Integer) jobDataMap.get("depth");
		long parentId = (Long) jobDataMap.get("parentId");
		String url = (String) jobDataMap.get("url");
		String uuID = (String) jobDataMap.get("uuID");
		int depthCount = (Integer) jobDataMap.get("depthCount");

		logger.info("execute()  {}", url);

		EntityManager em = PersistenceManager.getInstance().getEntityManager();
		try {
			boolean escapeFragment = false;
			String htmlPage = DownloadUtil.downloadPage(url, escapeFragment);

			List<String> allLinks = PageUtil.collectLinks(htmlPage);
			List<String> internalLinks = PageUtil.filterByInternalLinks(url, allLinks);
			
			int totalInternalLinks = internalLinks.size();
			int totalExternalLinks = allLinks.size() - internalLinks.size();
						
			Website website = null;
			WebsiteService websiteService = new WebsiteService();
			if (parentId == Website.PARENT_DEFAULT_ID) {
				website = websiteService.retreiveParent(em, uuID);
			}
			
			if (performValidation(em, websiteService, url, htmlPage)) {
				website = websiteService.create(em, parentId, url, htmlPage, uuID, depthCount, totalInternalLinks, totalExternalLinks);
			}

			if (website != null && (depthCount < definedDepth || definedDepth == 0)) {
				SchedulerService schedulerService = new SchedulerService();
				for (String link : internalLinks) {
					if (websiteService.isUrlNotExist(em, link) && HttpResponseUtil.getResponseCode(link) == HttpStatus.SC_OK) {
						schedulerService.schedulePersistSiteHierarchyJob(definedDepth, website.getId(), link, uuID, depthCount + 1);
					}
				}
			}
		} catch (Throwable e) {
			logger.warn("PersistSiteHierarchyJob: {}", e);
		} finally {
			PersistenceManager.closeEntityManager(em);
		}
	}
	
	private boolean performValidation(EntityManager em, WebsiteService websiteService, String url, String htmlPage) {
		String urlWithSlashAtEnd = url.endsWith("/") ? url : url + "/";
		return websiteService.isUrlNotExist(em, url) 
				&& websiteService.isUrlNotExist(em, urlWithSlashAtEnd) 
				&& DetectHtml.isHtml(htmlPage) 
				&& websiteService.isContentNotExist(em, htmlPage);
	}
}
