package com.ardor.job;

import java.io.Serializable;

import javax.persistence.EntityManager;

import org.apache.commons.lang3.StringUtils;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.impl.StdSchedulerFactory;
import org.quartz.impl.matchers.GroupMatcher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ardor.dao.CustomerWebsiteDAO;
import com.ardor.entity.CustomerWebsite;
import com.ardor.entity.Website;
import com.ardor.persistence.PersistenceManager;
import com.ardor.service.CustomerWebsiteService;
import com.ardor.service.WebsiteService;
import com.ardor.service.notification.NotificationService;
import com.ardor.service.scheduler.SchedulerService;

public class CrawlingDoneCheckerJob implements Serializable, Job {

	private static final long serialVersionUID = 1L;

	private static final Logger logger = LoggerFactory.getLogger(CrawlingDoneCheckerJob.class);

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		JobDataMap jobDataMap = context.getJobDetail().getJobDataMap();
		String uuID = (String) jobDataMap.get("uuID");
		
		boolean isDoneCrawling = true;
		try {
			Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();
			for (JobKey jobKey : scheduler.getJobKeys(GroupMatcher.jobGroupEquals(SchedulerService.CRAWLER_JOB_GROUP))) {
				if (StringUtils.endsWith(jobKey.toString(), uuID)) {
					isDoneCrawling = false;
				}
			}
		} catch (SchedulerException e) {
			logger.warn("JobChecker SchedulerException {}", e.getMessage());
		}
		
		if (isDoneCrawling) {
			EntityManager em = PersistenceManager.getInstance().getEntityManager();
			try {
				WebsiteService websiteService = new  WebsiteService();
				Website website = websiteService.retreiveParent(em, uuID);
				
				if (website != null) {
					websiteService.update(em, website);
					
					CustomerWebsite customerWebsite = new CustomerWebsiteDAO(em).findByUUID(uuID);
					customerWebsite.setDoneCrawling(true);
					new CustomerWebsiteService().updateCustomerWebsite(em, customerWebsite);
					
					new NotificationService().notifyCustomerDoneCrawling(customerWebsite.getCustomer(), website);
				}
			} catch (Throwable e) {
				logger.warn("CrawlJobChecker notify user {}", e.getMessage());
			} finally {
				PersistenceManager.closeEntityManager(em);
			}
		} else {
			new SchedulerService().scheduleCrawlingDoneChecker(uuID);
		}
	}
}
