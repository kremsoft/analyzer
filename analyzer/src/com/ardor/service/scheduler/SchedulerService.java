package com.ardor.service.scheduler;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;

import org.quartz.JobDataMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ardor.Const;
import com.ardor.dao.CustomerWebsiteDAO;
import com.ardor.dao.WebsiteDAO;
import com.ardor.entity.CustomerWebsite;
import com.ardor.entity.Website;
import com.ardor.job.CrawlingDoneCheckerJob;
import com.ardor.job.PersistSiteHierarchyJob;
import com.ardor.job.SendEmailJob;
import com.ardor.persistence.PersistenceManager;
import com.ardor.util.DownloadUtil;
import com.ardor.util.PageUtil;

public class SchedulerService extends AbstractSchedulerService {

	private static final long serialVersionUID = 1L;

	private static final String EMAIL_JOB_GROUP = "EMAIL_JOB_GROUP";

	public static final String CRAWLER_JOB_GROUP = "CRAWLER_JOB_GROUP";
	
	private static final String DONE_CRAWLING_CHECKER_JOB_GROUP = "CHECKER_JOB_GROUP";
	
	private static final Logger logger = LoggerFactory.getLogger(SchedulerService.class);

	// for sending email to single recipient
	public void scheduleSendEmail(String recipient, String subject, String message, String bcc) {
		List<String> recipients = new ArrayList<String>();
		recipients.add(recipient);
		scheduleSendEmail(recipients, subject, message, bcc);
	}

	// for sending email to multiple recipients, runs 3 seconds after method is called
	public void scheduleSendEmail(List<String> recipients, String subject, String message, String bcc) {
		JobDataMap jobDataMap = new JobDataMap();
		jobDataMap.put("recipients", recipients);
		jobDataMap.put("subject", subject);
		jobDataMap.put("message", message);
		jobDataMap.put("bcc", bcc);

		String jobName = SendEmailJob.class.getSimpleName() + System.nanoTime();
		Date triggerStartTime = new Date(System.currentTimeMillis() + (3 * Const.SECOND_IN_MILLISECONDS));
		scheduleJob(SendEmailJob.class, triggerStartTime, jobDataMap, jobName, EMAIL_JOB_GROUP);
	}

	public void schedulePersistSiteHierarchyJob(int depth, long parentId, String url, String uuID, int depthCount) {
		JobDataMap jobDataMap = new JobDataMap();
		jobDataMap.put("parentId", parentId);
		jobDataMap.put("url", url);
		jobDataMap.put("uuID", uuID);
		jobDataMap.put("depth", depth);
		jobDataMap.put("depthCount", depthCount);
				
		String jobName = PersistSiteHierarchyJob.class.getSimpleName() + System.nanoTime() + "-" + uuID;
		Date triggerStartTime = new Date(System.currentTimeMillis() + (2 * Const.SECOND_IN_MILLISECONDS));
		scheduleJob(PersistSiteHierarchyJob.class, triggerStartTime, jobDataMap, jobName, CRAWLER_JOB_GROUP);
	}
	
	public void scheduleCrawlingDoneChecker(String uuID) {
		JobDataMap jobDataMap = new JobDataMap();
		jobDataMap.put("uuID", uuID);
		
		String jobName = CrawlingDoneCheckerJob.class.getSimpleName() + System.nanoTime();
		Date triggerStartTime = new Date(System.currentTimeMillis() + (3 * Const.MINUTE_IN_MILLISECONDS));
		scheduleJob(CrawlingDoneCheckerJob.class, triggerStartTime, jobDataMap, jobName, DONE_CRAWLING_CHECKER_JOB_GROUP);
	}
	
	// TODO: check still processing
	public void reCrawlProcessing() {
		EntityManager em = PersistenceManager.getInstance().getEntityManager();
		try {
			List<CustomerWebsite> processingUsers = new CustomerWebsiteDAO(em).findProcessing();
			WebsiteDAO websiteDAO = new WebsiteDAO(em);
			for (CustomerWebsite user : processingUsers) {
				Website website = websiteDAO.findParentByUUID(user.getSiteUUID()); 
				boolean escapeFragment = false;
				String htmlPage = DownloadUtil.downloadPage(website.getUrl(), escapeFragment);
				
				List<String> allLinks = PageUtil.collectLinks(htmlPage);
				List<String> internalLinks = PageUtil.filterByInternalLinks(website.getUrl(), allLinks);
				
				for (String link : internalLinks) {
					schedulePersistSiteHierarchyJob(user.getSelectedDepth(), website.getId(), link, website.getUuID(), 1);
				}
				scheduleCrawlingDoneChecker(user.getSiteUUID());
			}
		} catch (Throwable e) {
			logger.warn("reCrawlProcessing() {}", e);
		} finally {
			PersistenceManager.closeEntityManager(em);
		}
	}
}
