package com.ardor.service;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import com.ardor.dao.CustomerWebsiteDAO;
import com.ardor.entity.Customer;
import com.ardor.entity.CustomerWebsite;

public class CustomerWebsiteService {
	
	public CustomerWebsite createCustomerWebsite(EntityManager em, Customer customer, String siteUUID, int selectedDepth) {
		CustomerWebsite customerWebsite = new CustomerWebsite();
		customerWebsite.setCustomer(customer);
		customerWebsite.setSiteUUID(siteUUID);
		customerWebsite.setSelectedDepth(selectedDepth);
		
		EntityTransaction et = em.getTransaction();
		et.begin();
		CustomerWebsite entity = new CustomerWebsiteDAO(em).create(customerWebsite);
		et.commit();
		
		return entity;
	}
	
	public boolean deleteCustomerWebsite(EntityManager em, String siteUUID) {
		CustomerWebsite customerWebsite = new CustomerWebsiteDAO(em).findByUUID(siteUUID);
		EntityTransaction et = em.getTransaction();
		et.begin();
		boolean isDeleted = new CustomerWebsiteDAO(em).delete(customerWebsite.getId());
		et.commit();
		return isDeleted;
	}
	
	public void updateCustomerWebsite(EntityManager em, CustomerWebsite customerWebsite) {
		EntityTransaction et = em.getTransaction();
		et.begin();
		new CustomerWebsiteDAO(em).update(customerWebsite);
		em.flush();
		em.clear();
		et.commit();
	}
}
