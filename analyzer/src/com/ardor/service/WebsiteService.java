package com.ardor.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import com.ardor.dao.CustomerWebsiteDAO;
import com.ardor.dao.WebsiteDAO;
import com.ardor.entity.CustomerWebsite;
import com.ardor.entity.Website;
import com.ardor.model.data.WebsiteModel;
import com.ardor.util.PageUtil;

public class WebsiteService {

	public Website create(EntityManager em, long parentId, String url, String htmlPage, String uuID, int depthCount, int totalInternalLinks, int totalExternalLinks) {
		Website website = new Website();
		website.setParentId(parentId);
		website.setUrl(url);
		website.setContent(htmlPage);
		website.setUuID(uuID);
		website.setDepthCount(depthCount);
		website.setTotalInternalLinks(totalInternalLinks);
		website.setTotalExternalLinks(totalExternalLinks);
		website.setContentCode(PageUtil.generateContentCode(htmlPage));
		website.setDateCrawled(Calendar.getInstance().getTime());
		
		EntityTransaction et = em.getTransaction();
		et.begin();
		Website entity = new WebsiteDAO(em).create(website);
		em.flush();
		em.clear();
		et.commit();
		
		return entity;
	}

	public void update(EntityManager em, Website website) {
		EntityTransaction et = em.getTransaction();
		et.begin();
		new WebsiteDAO(em).update(website);
		em.flush();
		em.clear();
		et.commit();
	}

	public boolean delete(EntityManager em, long id) {
		EntityTransaction et = em.getTransaction();
		et.begin();
		boolean isDeleted = new WebsiteDAO(em).delete(id);
		et.commit();
		return isDeleted;
	}
	
	public boolean deleteAllByUUID(EntityManager em, String uuID) {
		EntityTransaction et = em.getTransaction();
		et.begin();
		long rowsDeleted = new WebsiteDAO(em).deleteByUUID(uuID);
		et.commit();
		return rowsDeleted > 0 ? true : false;
	}
	
	public boolean isUrlNotExist(EntityManager em, String url) {
		List<Website> websites = new WebsiteDAO(em).findByURL(url);
		return websites.size() == 0 ? true : false;
	}
	
	public boolean isContentNotExist(EntityManager em, String htmlPage) {
		long contentCode = PageUtil.generateContentCode(htmlPage);
		List<Website> websites = new WebsiteDAO(em).findByContentCode(contentCode);
		return websites.size() == 0 ? true : false;
	}
	
	public Website retreiveParent(EntityManager em, String uuID) {
		List<Website> websites = new WebsiteDAO(em).findByUUID(uuID);
		return websites.get(0);
	}
	
	public List<WebsiteModel> buildDTO(EntityManager em, List<Website> websites) {
		List<WebsiteModel> websitesDTO = new ArrayList<>();
		CustomerWebsiteDAO userDAO = new CustomerWebsiteDAO(em);
		for (Website website : websites) {
			CustomerWebsite user = userDAO.findByUUID(website.getUuID());
			websitesDTO.add( new WebsiteModel(website, user.isDoneCrawling()));
		}
		return websitesDTO;
	}
}
