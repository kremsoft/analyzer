package com.ardor.service.notification;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Serializable;
import java.io.Writer;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ardor.service.scheduler.SchedulerService;
import com.ardor.util.StringUtil;

import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.Template;
import freemarker.template.TemplateException;

public abstract class AbstractNotificationService implements Serializable {

	private static final long serialVersionUID = 1L;

	private static final Logger logger = LoggerFactory.getLogger(AbstractNotificationService.class);

	protected final static String SUBJECT = "Subject";
	protected final static String MESSAGE = "Message";

	protected final static String PARAM_SUBJECT = "subject";
	protected final static String PARAM_MESSAGE = "message";

	protected Configuration configuration;

	public AbstractNotificationService() {
		String absolutePath = new File(Thread.currentThread().getContextClassLoader().getResource("").getFile()).getParentFile().getParentFile().getPath(); // this goes to webapps directory
		try {
			absolutePath = absolutePath.replaceAll("%20", " ");
			File directory = new File(absolutePath + File.separator + "templates");

			configuration = new Configuration(Configuration.VERSION_2_3_23);
			configuration.setDirectoryForTemplateLoading(directory);
			configuration.setObjectWrapper(new DefaultObjectWrapper(Configuration.VERSION_2_3_23));
		} catch (IOException e) {
			throw new RuntimeException(e); // stop the application
		}
	}

	protected void send(List<String> recipients, String templateName, Map<String, Object> params) {
		String subject = getContentFromTemplate(getSubject(templateName), params);
		String message = getContentFromTemplate(getMessageHTML(templateName), params);
		if (message != null & subject != null) {
			new SchedulerService().scheduleSendEmail(recipients, subject, message, null);
		}
	}

	protected String getRawTemplate(String templateName) {
		try {
			Template template = this.configuration.getTemplate(templateName);
			return template.toString();
		} catch (IOException e) {
			logger.error(e.getMessage(), e);
		}
		return StringUtil.EMPTY_STRING;
	}

	protected String getContentFromTemplate(String templateName, Map<String, Object> params) {
		String result = null;
		Writer out = null;
		ByteArrayOutputStream bos = null;
		try {
			if (this.configuration == null) {
				logger.error("UNABLE TO LOAD TEMPLATE");
				return null;
			}
			Template template = this.configuration.getTemplate(templateName);

			bos = new ByteArrayOutputStream();
			out = new OutputStreamWriter(bos);
			template.process(params, out);

			bos.flush();
			out.flush();
			result = bos.toString();
		} catch (IOException e) {
			logger.error(e.getMessage(), e);
		} catch (TemplateException e) {
			logger.error(e.getMessage(), e);
		} finally {
			if (out != null) {
				try {
					out.close();
				} catch (IOException e) {
					logger.error(e.getMessage(), e);
				}
			}
			if (bos != null) {
				try {
					bos.close();
				} catch (IOException e) {
					logger.error(e.getMessage(), e);
				}
			}
		}
		return result;
	}

	protected String getSubject(String templateName) {
		return String.format(templateName, SUBJECT);
	}

	protected String getMessageHTML(String templateName) {
		return String.format(templateName, MESSAGE);
	}
}
