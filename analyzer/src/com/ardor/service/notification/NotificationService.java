package com.ardor.service.notification;

import java.util.HashMap;
import java.util.Map;

import com.ardor.entity.Customer;
import com.ardor.entity.Website;
import com.ardor.service.scheduler.SchedulerService;
import com.ardor.util.DomainUtil;
import com.ardor.util.StringUtil;

public class NotificationService extends AbstractNotificationService {

	private static final long serialVersionUID = 1L;

	private final static String CRAWLING_DONE_TEMPLATE = "CrawlingDone%s.ftl";
	
	private final static String PASSWORD_SEND_TEMPLATE = "SendPassword%s.ftl";

	public void notifyCustomerDoneCrawling(Customer customer, Website website) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("websiteURL", DomainUtil.extractDomain(website.getUrl()));
		paramMap.put("url", "http://198.1.120.98:8080/internal-linking-tool/search-keywords?websiteId=" + website.getId());
		paramMap.put("password", customer.getPasswordText());

		String subject = getContentFromTemplate(getSubject(CRAWLING_DONE_TEMPLATE), paramMap);
		String message = getContentFromTemplate(getMessageHTML(CRAWLING_DONE_TEMPLATE), paramMap);
		if (message != null & subject != null) {
			String bcc = StringUtil.EMPTY_STRING;
			new SchedulerService().scheduleSendEmail(customer.getEmail(), subject, message, bcc);
		}
	}
	
	public void notifyCustomerPassword(String email, String password) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("password", password);
		paramMap.put("url", "http://198.1.120.98:8080/internal-linking-tool/customer-login");
		
		String subject = getContentFromTemplate(getSubject(PASSWORD_SEND_TEMPLATE), paramMap);
		String message = getContentFromTemplate(getMessageHTML(PASSWORD_SEND_TEMPLATE), paramMap);
		if (message != null & subject != null) {
			String bcc = StringUtil.EMPTY_STRING;
			new SchedulerService().scheduleSendEmail(email, subject, message, bcc);
		}
	}
}
