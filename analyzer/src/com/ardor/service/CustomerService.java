package com.ardor.service;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Calendar;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceException;

import org.apache.commons.lang3.RandomStringUtils;

import com.ardor.dao.CustomerDAO;
import com.ardor.entity.Customer;
import com.ardor.security.Pbkdf2Cipher;

public class CustomerService {
	
	public Customer createCustomer(EntityManager em, String email, String generatedPassword) throws PersistenceException {
		CustomerDAO customerDAO = new CustomerDAO(em);
		
		if (customerDAO.findByEmail(email) != null) {
			throw new PersistenceException("Email already exist");
		}
		
		Customer customer = new Customer(email);
		customer.setDateCreated(Calendar.getInstance().getTime());
		ecryptPassword(customer, generatedPassword);
		
		EntityTransaction et = em.getTransaction();
		et.begin();
		Customer entity = customerDAO.create(customer);
		em.flush();
		em.clear();
		et.commit();
		
		return entity;
	}
	
	private void ecryptPassword(Customer customer, String generatedPassword) {
		
		Pbkdf2Cipher cipher = new Pbkdf2Cipher();
		try {
			cipher.encrypt(generatedPassword);
		} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
			throw new RuntimeException(e);
		}

		String encryptedPassword = cipher.getEncryptedString();
		String passwordSalt = cipher.getSaltString();
		
		customer.setPassword(encryptedPassword);
		customer.setPasswordSalt(passwordSalt);
		customer.setPasswordText(generatedPassword);
	}
	
	public String generateRandomPassword() {
		return RandomStringUtils.randomAlphanumeric(12);
	}
	
	public Customer updatePassword(EntityManager em, Customer customer) {
		String generatedPassword = generateRandomPassword();
		ecryptPassword(customer, generatedPassword);
		
		EntityTransaction et = em.getTransaction();
		et.begin();
		Customer entity = new CustomerDAO(em).update(customer);
		em.flush();
		em.clear();
		et.commit();
		
		return entity;
	}
}
