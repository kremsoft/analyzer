package com.ardor.util;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;

import javax.net.ssl.SSLContext;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContexts;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ardor.exception.PageNotFoundException;

public class DownloadUtil {

	private static final Logger logger = LoggerFactory.getLogger(DownloadUtil.class);

	private final static String ESCAPE_FRAGMENT = "?_escaped_fragment_="; // ajax escaped fragment https://developers.google.com/webmasters/ajax-crawling/docs/getting-started
	
	private final static String USER_AGENT = "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.0";
	
	// TODO: implement another techniques here
	
	public static String downloadPage(String urlString, boolean escapeFragment) throws IOException, PageNotFoundException {
		String html = StringUtil.EMPTY_STRING;

		String downloadUrl = urlString;
		if (escapeFragment) {
			if (urlString.contains("#!")) {
				downloadUrl = urlString.replace("#!", ESCAPE_FRAGMENT);
			} else if (!urlString.contains("?")) {
				downloadUrl = urlString.concat(ESCAPE_FRAGMENT);
			}
		}

		SSLConnectionSocketFactory sslsf = null;
		try {
			SSLContext sslContext = SSLContexts.custom().loadTrustMaterial(new TrustSelfSignedStrategy()).build();
			sslsf = new SSLConnectionSocketFactory(sslContext, NoopHostnameVerifier.INSTANCE);
		} catch (KeyManagementException | NoSuchAlgorithmException | KeyStoreException e) {
			logger.warn("Using SSLContexts.createSystemDefault()");
			SSLContext sslContext = SSLContexts.createSystemDefault();
			sslsf = new SSLConnectionSocketFactory(sslContext, NoopHostnameVerifier.INSTANCE);
		}

		CloseableHttpClient httpClient = HttpClients.custom().setUserAgent(USER_AGENT).disableAutomaticRetries().disableCookieManagement().setSSLSocketFactory(sslsf).build();
	
		HttpGet httpGet = null;
		try {
			httpGet = new HttpGet(downloadUrl);
			// logger.debug("Using HttpClient '{}'", httpGet.getRequestLine());

			ResponseHandler<String> responseHandler = new ResponseHandler<String>() {
				@Override
				public String handleResponse(final HttpResponse response) throws ClientProtocolException, IOException {
					if (StringUtils.containsIgnoreCase(response.getLastHeader("Content-Type").getValue(), "image")) {
						return StringUtil.EMPTY_STRING;
					}
					
					StatusLine statusLine = response.getStatusLine();
					int status = statusLine.getStatusCode();
					if (status >= 200 && status < 300) {
						HttpEntity entity = response.getEntity();
						return entity != null ? EntityUtils.toString(entity) : StringUtil.EMPTY_STRING;
					} else {
						throw new ClientProtocolException("Connection error (" + status + " - " + statusLine.getReasonPhrase() + ")");
					}
				}
			};

			html = httpClient.execute(httpGet, responseHandler);
		} catch (ClientProtocolException cpe) {
			// re-throw as PageNotFoundException
			throw new PageNotFoundException(cpe.getMessage());
		} finally {
			logger.debug("httpClient.close()");
			if (httpGet != null) { 
				httpGet.releaseConnection(); 
			}
			httpClient.close();
		}
		return html;
	}

	public static void main(String[] args) throws IOException, PageNotFoundException {
		boolean escapeFragment = false;
		String url = "https://ardorseo.com/wp-content/uploads/2015/01/bundles.jpg";
		String htmlPage = DownloadUtil.downloadPage(url, escapeFragment);
		
		System.out.println("-" + htmlPage);
	}
}
