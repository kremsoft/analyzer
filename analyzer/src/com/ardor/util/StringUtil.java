package com.ardor.util;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class StringUtil {

	public static final String EMPTY_STRING = "";

	public static int stringToInt(String s, int defaultValue) {
		try {
			return (s != null) ? Integer.parseInt(s.replaceAll("[,]", "")) : defaultValue;
		} catch (NumberFormatException nfe) {
			return defaultValue;
		}
	}

	public static long stringToLong(String s, long defaultValue) {
		try {
			return (s != null) ? Long.parseLong(s.replaceAll("[,]", "")) : defaultValue;
		} catch (NumberFormatException nfe) {
			return defaultValue;
		}
	}

	public static float stringToFloat(String s, float defaultValue) {
		try {
			return (s != null) ? Float.parseFloat(s.replaceAll("[,]", "")) : defaultValue;
		} catch (NumberFormatException nfe) {
			return defaultValue;
		}
	}

	public static double stringToDouble(String s, double defaultValue) {
		try {
			return (s != null) ? Double.parseDouble(s.replaceAll("[,]", "")) : defaultValue;
		} catch (NumberFormatException nfe) {
			return defaultValue;
		}
	}

	public static BigDecimal stringToBigDecimal(String s, String defaultValue) {
		try {
			if (s != null) {
				Double.parseDouble(s.replaceAll("[,]", ""));
				return new BigDecimal(s.replaceAll("[,]", ""));
			}
		} catch (NumberFormatException nfe) {
			// do nothing
		}
		return new BigDecimal(defaultValue);
	}

	public static Date stringToDate(String s, String format) {
		try {
			return (s != null) ? new SimpleDateFormat(format).parse(s) : null;
		} catch (Exception e) {
			// do nothing
		}
		return null;
	}

	public static String truncate(String s, int length) {
		if (s != null && s.length() > length) {
			return s.substring(0, length);
		}
		return s;
	}

	public static List<String> stringToParsedList(String s, String separator) {
		List<String> parsedList = new ArrayList<String>(10);

		if (s != null && !s.isEmpty()) {
			String[] stringArray = s.split(separator);
			if (stringArray != null) {
				for (String element : stringArray) {
					parsedList.add(element.trim());
				}
			}
		}

		return parsedList;
	}
}
