package com.ardor.util;

import java.util.List;

import javax.mail.internet.InternetAddress;

import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ardor.Const;

public class SendEmailUtil {

	private static final Logger logger = LoggerFactory.getLogger(SendEmailUtil.class);

	private static String SMTP_HOST = "mail.ardormediafactory.com";
	private static String SMTP_USER = "support@ardormediafactory.com";
	private static String SMTP_PASS = "Z1vy@DgDhahM";
	private static String CHARSET = "UTF-8";
	private static int SMTP_PORT = 26;

	public static void send(List<String> recipients, String subject, String message, String bcc) throws EmailException {
		HtmlEmail htmlEmail = new HtmlEmail();

		htmlEmail.setHostName(SMTP_HOST);
		htmlEmail.setAuthentication(SMTP_USER, SMTP_PASS);

		htmlEmail.setFrom("support@ardormediafactory.com", "Ardor Media Factory");

		htmlEmail.setSubject(subject);
		htmlEmail.setHtmlMsg(message);
		htmlEmail.setCharset(CHARSET);
		htmlEmail.setSmtpPort(SMTP_PORT);

		for (String recipient : recipients) {
			htmlEmail.addTo(recipient);
		}

		if (bcc != null && !bcc.isEmpty()) {
			htmlEmail.addBcc(bcc);
		}
		
		if (Const.REMOTE) {
			htmlEmail.send();
			logger.info("Sending on live ...");
		}
		
		StringBuilder sb = new StringBuilder();
		for (InternetAddress i : htmlEmail.getToAddresses()) {
			if (sb.length() > 0) {
				sb.append(", ");
			}
			sb.append(i.getAddress());
		}

		logger.info("Subject {} ", htmlEmail.getSubject());
		logger.info("Recipients {}", sb.toString());
		logger.info("Email {}", message);
	}
	
	public static boolean isValidEmail(String email) {
		 return email.contains("@") && email.contains(".");
	}
}
