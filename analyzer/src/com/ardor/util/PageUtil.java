package com.ardor.util;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ardor.entity.Website;
import com.ardor.exception.PageNotFoundException;

public class PageUtil {
	
	private static final Logger logger = LoggerFactory.getLogger(PageUtil.class);

	public static List<String> collectLinks(String htmlPage) {
		Set<String> pageLinks = new HashSet<>();
		Document document = Jsoup.parse(htmlPage);
		List<Element> links = document.select("a[href]");
		for (Element element : links) {
			String link = element.attr("href").trim();
			if (!link.isEmpty()) {
				pageLinks.add(link);
			}
		}
		return new ArrayList<>(pageLinks);
	}
	
	public static List<String> filterByInternalLinks(String url, List<String> allLinks) {
		List<String> filteredLinks = new ArrayList<>();
		for (String link : allLinks) {
			try {
				URL linkUrl = new URL(new URL(url), link);
				if (DomainUtil.isSameDomain(url, linkUrl.toString())) {
					filteredLinks.add(linkUrl.toString());
				}
			} catch (Exception e) {
				logger.warn("Url: " + link + " " + e.getMessage());
			}
		}
		return filteredLinks;
	}
	
	public static long generateContentCode(String htmlPage) {
		return htmlPage.length();
	}
	
	public static List<Website> filterKeyword(String keyword, List<Website> websites) {
		List<Website> filteredSites = new ArrayList<>();
		for (Website website : websites) {
			Document document = Jsoup.parse(website.getContent());
			document.getElementsByTag("a").remove();
			document.getElementsByTag("head").remove();
			document.getElementsByTag("footer").remove();
			document.getElementsByTag("form").remove();
			document.getElementsByTag("button").remove();
			
			String documentText = document.text();
			if (StringUtils.containsIgnoreCase(documentText, keyword)) {
				filteredSites.add(website);
			}
		}
		return filteredSites;
	}
	
	public static void main(String[] args) throws IOException, PageNotFoundException {
		String urlString = "http://www.widriglaw.com/make-a-payment.html";
		
		String html = DownloadUtil.downloadPage(urlString, false);
		Document document = Jsoup.parse(html);
		document.getElementsByTag("a").remove();
		document.getElementsByTag("head").remove();
		document.getElementsByTag("footer").remove();
		document.getElementsByTag("form").remove();
		document.getElementsByTag("button").remove();
		
		System.out.println(document.text());
		String keyword = "divorce";
		System.out.println(StringUtils.containsIgnoreCase(document.text(), keyword ));
	}
}
