package com.ardor.util;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;

import javax.net.ssl.SSLContext;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContexts;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HttpResponseUtil {

	private static final Logger logger = LoggerFactory.getLogger(HttpResponseUtil.class);
	
	private final static String USER_AGENT = "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.0";
	
	public static int getResponseCode(String url) throws IOException {
		
		SSLConnectionSocketFactory sslsf = null;
		try {
			SSLContext sslContext = SSLContexts.custom().loadTrustMaterial(new TrustSelfSignedStrategy()).build();
			sslsf = new SSLConnectionSocketFactory(sslContext, NoopHostnameVerifier.INSTANCE);
		} catch (KeyManagementException | NoSuchAlgorithmException | KeyStoreException e) {
			logger.warn("Using SSLContexts.createSystemDefault()");
			SSLContext sslContext = SSLContexts.createSystemDefault();
			sslsf = new SSLConnectionSocketFactory(sslContext, NoopHostnameVerifier.INSTANCE);
		}
		
		int statusCode = 0;
		HttpGet httpGet = null;
		
		CloseableHttpClient httpClient = HttpClients.custom().setUserAgent(USER_AGENT).disableAutomaticRetries().disableCookieManagement().setSSLSocketFactory(sslsf).disableRedirectHandling().build();
		CloseableHttpResponse response = null;
		try {
			httpGet = new HttpGet(url);
			response = httpClient.execute(httpGet);
			statusCode = response.getStatusLine().getStatusCode();
			logger.debug("testResponseCode for url: {} : {}", url, statusCode);
		} finally {
			httpClient.close();	
			if (httpGet != null) {
				httpGet.releaseConnection();
			}
			
			if (response != null) {
				response.close();
			}
		}
		return statusCode;
	}
}
