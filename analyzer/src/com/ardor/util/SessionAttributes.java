package com.ardor.util;

public class SessionAttributes {
	
	public static final String SESSION_CUSTOMER = "SESSION_CUSTOMER";
	
	public static final String SESSION_USER_AUTHENTICATION_MODE = "SESSION_USER_AUTHENTICATION_MODE";
	
	public static final String SESSION_USER_TYPE = "SESSION_USER_TYPE";
	
	public enum UserType {

		CUSTOMER("CUSTOMER");
		
		private String description;
		
		private UserType(String description) {
			this.description = description;
		}
		
		public String getDescription() {
			return description;
		}
	}
}
