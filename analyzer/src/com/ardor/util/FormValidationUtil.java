package com.ardor.util;

import java.io.IOException;

import org.apache.commons.validator.routines.EmailValidator;
import org.apache.http.HttpStatus;

import com.ardor.exception.FormValidationException;

public class FormValidationUtil {
	
	public static void validateEmail(String email) throws FormValidationException {
		if ( ! EmailValidator.getInstance().isValid(email)) {
			throw new FormValidationException("Email must be valid.");
		}
	}
	
	public static  void validateUrl(String url) throws FormValidationException {
		if (url.isEmpty()) {
			throw new FormValidationException("URL must not be empty.");
		}
		
		try {
			int responseCode = HttpResponseUtil.getResponseCode(url);
			if (responseCode != HttpStatus.SC_OK) {
				throw new FormValidationException("Url status is " + responseCode);
			}
		} catch (IOException e) {
			throw new FormValidationException("URL status is not OK " + e.getMessage());
		}
	}
}
