package com.ardor.util;

import java.net.MalformedURLException;
import java.net.URL;

import org.apache.commons.lang3.StringUtils;

public class DomainUtil {

	public static String extractDomain(String url) {
		try {
			return removeProtocal(url);
		} catch (MalformedURLException e) {
			return StringUtil.EMPTY_STRING;
		}
	}

	private static String removeProtocal(String url) throws MalformedURLException {
		String host = new URL(url).getHost();
		return host.replaceAll("www.", "");
	}
	

	public static boolean isSameDomain(String firstUrl, String secondUrl) {
		String firstUrlDomain = extractDomain(firstUrl);
		String secondUrlDomain = extractDomain(secondUrl);
		
		return StringUtils.equalsIgnoreCase(firstUrlDomain, secondUrlDomain);
	}
}
