package com.ardor.dao;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.ardor.entity.Customer;

public class CustomerDAO extends AbstractDAO<Customer> {

	public CustomerDAO(EntityManager em) {
		super(em, Customer.class);
	}
	
	public Customer findByEmail(String email) {
		String sql = "SELECT e FROM Customer e WHERE e.email = :email";
		Query query = em.createQuery(sql);
		query.setParameter("email", email);
		return getSingleResult(query);
	}
}
