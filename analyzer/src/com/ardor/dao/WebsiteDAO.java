package com.ardor.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.ardor.entity.Website;

public class WebsiteDAO extends AbstractDAO<Website> {

	public WebsiteDAO(EntityManager em) {
		super(em, Website.class);
	}

	public List<Website> findByURL(String url) {
		String sql = "SELECT e FROM Website e WHERE e.url = :url";
		Query query = em.createQuery(sql);
		query.setParameter("url", url);
		return getResultList(query);
	}

	public List<Website> findByContentCode(long contentCode) {
		String sql = "SELECT e FROM Website e WHERE e.contentCode = :contentCode";
		Query query = em.createQuery(sql);
		query.setParameter("contentCode", contentCode);
		return getResultList(query);
	}
	
	public List<Website> findByDomainName(String domainName) {
		String sql = "SELECT e FROM Website e WHERE e.url LIKE :domainName";
		Query query = em.createQuery(sql);
		query.setParameter("domainName", "%" + domainName + "%");
		return getResultList(query);
	}

	public List<Website> findByParentId(long parentId, int firstResult, int maxResult) {
		String sql = "SELECT e FROM Website e WHERE e.parentId = :parentId ORDER BY e.dateCrawled DESC";
		Query query = em.createQuery(sql);
		query.setParameter("parentId", parentId);
		query.setFirstResult(firstResult);
		query.setMaxResults(maxResult);
		return getResultList(query);
	}
	
	public List<Website> findByParentId(long parentId, List<String> uuIDs, int firstResult, int maxResult) {
		String sql = "SELECT e FROM Website e WHERE e.parentId = :parentId AND e.uuID IN :uuIDs ORDER BY e.dateCrawled DESC";
		Query query = em.createQuery(sql);
		query.setParameter("parentId", parentId);
		query.setParameter("uuIDs", uuIDs);
		query.setFirstResult(firstResult);
		query.setMaxResults(maxResult);
		return getResultList(query);
	}
	
	public List<Website> findByUUID(String uuID) {
		String sql = "SELECT e FROM Website e WHERE e.uuID = :uuID ORDER BY e.parentId ASC";
		Query query = em.createQuery(sql);
		query.setParameter("uuID", uuID);
		return getResultList(query);
	}

	public List<Website> findByKeyword(String keyword, String uuID) {
		// String sql = "SELECT * FROM website WHERE MATCH(content) AGAINST (? IN BOOLEAN MODE) AND content LIKE ? AND uuID = ? ORDER BY depthCount, id;";
		String sql = "SELECT * FROM website WHERE content LIKE ? AND uuID = ? ORDER BY depthCount, id;";
		
		Query query = em.createNativeQuery(sql, Website.class);
		query.setParameter(1, "%" + keyword + "%");
		query.setParameter(2, uuID);
		return getResultList(query);
	}

	public long countByParentId(long parentId) {
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT COUNT(e) FROM Website e WHERE e.parentId = :parentId");
		Query query = em.createQuery(sql.toString());
		query.setParameter("parentId", parentId);
		return (long) query.getSingleResult();
	}
	
	public long countByParentId(long parentId, List<String> uuIDs) {
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT COUNT(e) FROM Website e WHERE e.parentId = :parentId AND e.uuID IN :uuIDs");
		Query query = em.createQuery(sql.toString());
		query.setParameter("parentId", parentId);
		query.setParameter("uuIDs", uuIDs);
		return (long) query.getSingleResult();
	}

	public long deleteByUUID(String uuID) {
		String sql = "DELETE FROM Website WHERE uuID = :uuID";
		Query query = em.createQuery(sql);
		query.setParameter("uuID", uuID);
		return query.executeUpdate();
	}
	
	public Website findLastCrawledDateByUUID(String uuID) {
		String sql = "SELECT e FROM Website e WHERE e.uuID = :uuID ORDER BY e.dateCrawled DESC";
		Query query = em.createQuery(sql);
		query.setParameter("uuID", uuID);
		query.setMaxResults(1);
		return getSingleResult(query);
	}
	
	public Website findParentByUUID(String uuID) {
		String sql = "SELECT e FROM Website e WHERE e.uuID = :uuID AND e.parentId = 0";
		Query query = em.createQuery(sql);
		query.setParameter("uuID", uuID);
		return getSingleResult(query);
	}
}
