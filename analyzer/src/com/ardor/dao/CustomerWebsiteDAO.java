package com.ardor.dao;

import java.util.LinkedList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.ardor.entity.CustomerWebsite;

public class CustomerWebsiteDAO extends AbstractDAO<CustomerWebsite> {

	public CustomerWebsiteDAO(EntityManager em) {
		super(em, CustomerWebsite.class);
	}
	
	public CustomerWebsite findByUUID(String siteUUID) {
		String sql = "SELECT e FROM CustomerWebsite e WHERE e.siteUUID = :siteUUID";
		Query query = em.createQuery(sql);
		query.setParameter("siteUUID", siteUUID);
		return getSingleResult(query);
	}
	
	public List<CustomerWebsite> findProcessing() {
		String sql = "SELECT e FROM CustomerWebsite e WHERE e.isDoneCrawling = FALSE";
		Query query = em.createQuery(sql);
		return getResultList(query);
	}
	
	public List<String> findUUIDs(long customerID) {
		String sql = "SELECT e FROM CustomerWebsite e WHERE e.customer.id = :customerID";
		Query query = em.createQuery(sql);
		query.setParameter("customerID", customerID);
		List<String> uuIDs = new LinkedList<>();
		for (CustomerWebsite customerWebsiteObject : getResultList(query)) {
			uuIDs.add(customerWebsiteObject.getSiteUUID());
		}
		return uuIDs;
	}
}
