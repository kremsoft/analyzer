package com.ardor;

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Const {

	private static final Logger logger = LoggerFactory.getLogger(Const.class);

	public final static long MILLISECONDS = 1000;
	public final static long SECOND_IN_MILLISECONDS = 1 * MILLISECONDS;
	public final static long MINUTE_IN_MILLISECONDS = 60 * SECOND_IN_MILLISECONDS;
	public final static long HOUR_IN_MILLISECONDS = 60 * MINUTE_IN_MILLISECONDS;
	public final static long DAY_IN_MILLISECONDS = 24 * HOUR_IN_MILLISECONDS;

	public static boolean REMOTE = true;
	public static boolean DEBUG = false;
	
	public static String SERVER_URL = "http://198.1.120.98:8080/internal-linking-tool";

	static {
		// http://stackoverflow.com/questions/7615645/ssl-handshake-alert-unrecognized-name-error-since-upgrade-to-java-1-7-0/14884941#14884941
		System.setProperty("jsse.enableSNIExtension", "false");

		try {
			REMOTE = !InetAddress.getLocalHost().isSiteLocalAddress();
			if ( ! REMOTE) {
				SERVER_URL = "http://localhost:8080/internal-linking-tool";
			}
			logger.info("REMOTE = {}", REMOTE);
		} catch (UnknownHostException uhe) {
			logger.error(uhe.getMessage(), uhe);
		}
	}
}
