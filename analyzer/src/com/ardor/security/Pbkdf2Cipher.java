package com.ardor.security;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

import org.apache.commons.codec.binary.Base64;

/**
 * @author rmagallen
 */
public class Pbkdf2Cipher {

	private static String cipherAlgorithm = "PBKDF2WithHmacSHA1";
	private static String hashAlgorithm = "SHA1PRNG";
	private static int derivedKeyLength = 160;
	private static int iterations = 10000;
	private String encryptedString;
	private String saltString;

	public void encrypt(String plainString) throws NoSuchAlgorithmException, InvalidKeySpecException {
		encrypt(plainString, null);
	}

	public void encrypt(String plainString, String suppliedSaltString)
			throws NoSuchAlgorithmException, InvalidKeySpecException {
		Base64 base64 = new Base64(32, new byte[] {}, true);
		byte[] salt = new byte[8];
		if (suppliedSaltString != null) {
			saltString = suppliedSaltString;
			salt = base64.decode(saltString);
		} else {
			SecureRandom random = SecureRandom.getInstance(hashAlgorithm);
			random.nextBytes(salt);
			saltString = new String(base64.encode(salt));
		}
		// implement pbkdf2
		KeySpec spec = new PBEKeySpec(plainString.toCharArray(), salt, iterations, derivedKeyLength);
		SecretKeyFactory f = SecretKeyFactory.getInstance(cipherAlgorithm);
		byte[] b = f.generateSecret(spec).getEncoded();
		// convert to base64
		encryptedString = new String(base64.encode(b));
	}

	public String getEncryptedString() {
		return encryptedString;
	}

	public String getSaltString() {
		return saltString;
	}
}
