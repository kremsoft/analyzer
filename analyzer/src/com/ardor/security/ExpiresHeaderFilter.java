package com.ardor.security;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ardor.util.StringUtil;

/**
 * @author rmagallen
 */
public class ExpiresHeaderFilter implements Filter {

	private static final Logger logger = LoggerFactory.getLogger(ExpiresHeaderFilter.class);
	private FilterConfig filterConfig = null;

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		long expiresDuration = StringUtil.stringToLong(filterConfig.getInitParameter("expires.duration"), 0);
		((HttpServletResponse) response).setDateHeader("Expires", System.currentTimeMillis() + expiresDuration);
		chain.doFilter(request, response);
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		this.filterConfig = filterConfig;
	}

	@Override
	public void destroy() {
		logger.debug("{} destroy()", filterConfig.getFilterName());
	}
}
