package com.ardor.security;

import java.io.IOException;
import java.net.URLEncoder;

import javax.faces.context.FacesContext;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ardor.entity.Customer;
import com.ardor.util.FacesUtil;
import com.ardor.util.SessionAttributes;
import com.ocpsoft.pretty.PrettyContext;

/**
 * @author rmagallen
 */
public class SecurityFilter implements Filter {

	private static final Logger logger = LoggerFactory.getLogger(SecurityFilter.class);
	
	private FilterConfig filterConfig = null;

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		FacesContext facesContext = FacesUtil.getFacesContext((HttpServletRequest) request, (HttpServletResponse) response);
		String fromUrl = PrettyContext.getCurrentInstance().getRequestURL().toURL();
		fromUrl += URLEncoder.encode(PrettyContext.getCurrentInstance().getRequestQueryString().toQueryString(), "UTF-8");
		facesContext.release();

		String uri = ((HttpServletRequest) request).getRequestURI();
		// logger.debug("SecurityFilter " + uri);
		
		if (uri.contains("/internal-linking-tool/secure/searchKeywords") || uri.contains("/internal-linking-tool/secure/websiteCrawler")) {
			HttpSession session = ((HttpServletRequest) request).getSession();
			Customer customer = (Customer) session.getAttribute(SessionAttributes.SESSION_CUSTOMER);
			if (customer == null) {
				String redirectUrl = ((HttpServletRequest) request).getContextPath() + "/customer-login?from=" + fromUrl;
				((HttpServletResponse) response).sendRedirect(redirectUrl);
				((HttpServletResponse) response).setStatus(HttpServletResponse.SC_MOVED_PERMANENTLY);
				((HttpServletResponse) response).setHeader("Location", redirectUrl);
				return;
			}
			
		}
		chain.doFilter(request, response);
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		this.filterConfig = filterConfig;
	}

	@Override
	public void destroy() {
		logger.debug("{} destroy()", filterConfig.getFilterName());
	}
}
