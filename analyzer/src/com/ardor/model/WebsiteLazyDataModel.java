package com.ardor.model;

import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import com.ardor.dao.WebsiteDAO;
import com.ardor.entity.Website;
import com.ardor.model.data.WebsiteModel;
import com.ardor.persistence.PersistenceManager;
import com.ardor.service.WebsiteService;

public class WebsiteLazyDataModel extends LazyDataModel<WebsiteModel> {

	private static final long serialVersionUID = 1L;
	
	private List<String> uuIDs;
	
	public WebsiteLazyDataModel(List<String> uuIDs) {
		this.uuIDs = uuIDs;
	}

	@Override
	public List<WebsiteModel> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
		EntityManager em = PersistenceManager.getInstance().getEntityManager();
		try {
			WebsiteDAO websiteDAO = new WebsiteDAO(em);
			List<Website> websites = websiteDAO.findByParentId(Website.PARENT_DEFAULT_ID, uuIDs, first, pageSize);
			setRowCount((int) websiteDAO.countByParentId(Website.PARENT_DEFAULT_ID, uuIDs));
			return new WebsiteService().buildDTO(em, websites);
		} finally {
			PersistenceManager.closeEntityManager(em);
		}
	}

	public List<String> getUuIDs() {
		return uuIDs;
	}

	public void setUuIDs(List<String> uuIDs) {
		this.uuIDs = uuIDs;
	}
}
