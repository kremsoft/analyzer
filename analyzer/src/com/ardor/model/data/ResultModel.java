package com.ardor.model.data;

import java.io.Serializable;
import java.util.List;

import com.ardor.entity.Website;

public class ResultModel implements Serializable {

	private static final long serialVersionUID = 1L;

	private String keyword;

	private List<Website> websites;

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public List<Website> getWebsites() {
		return websites;
	}

	public void setWebsites(List<Website> websites) {
		this.websites = websites;
	}
}
