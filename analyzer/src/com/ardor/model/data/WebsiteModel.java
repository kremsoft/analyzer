package com.ardor.model.data;

import java.io.Serializable;
import java.util.Date;

import com.ardor.entity.Website;

public class WebsiteModel implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private long id;

	private String url;
	
	private String uuID;
	
	private Date dateCrawled;
	
	private boolean isDoneCrawling;
	
	public WebsiteModel(Website website, boolean isDoneCrawling) {
		this.id = website.getId();
		this.url = website.getUrl();
		this.uuID = website.getUuID();
		this.dateCrawled = website.getDateCrawled();
		this.isDoneCrawling = isDoneCrawling;
	}

	public long getId() {
		return id;
	}

	public String getUrl() {
		return url;
	}

	public String getUuID() {
		return uuID;
	}

	public Date getDateCrawled() {
		return dateCrawled;
	}

	public boolean isDoneCrawling() {
		return isDoneCrawling;
	}
}
