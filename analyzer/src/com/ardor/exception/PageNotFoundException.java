package com.ardor.exception;

public class PageNotFoundException extends Exception {

	private static final long serialVersionUID = 1L;

	public PageNotFoundException(String message) {
		super(message);
	}
}
