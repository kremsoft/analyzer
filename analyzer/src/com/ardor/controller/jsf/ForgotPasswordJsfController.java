package com.ardor.controller.jsf;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ardor.dao.CustomerDAO;
import com.ardor.entity.Customer;
import com.ardor.exception.FormValidationException;
import com.ardor.persistence.PersistenceManager;
import com.ardor.service.CustomerService;
import com.ardor.service.notification.NotificationService;
import com.ardor.util.FormValidationUtil;

@ManagedBean(name = "forgotPasswordJsfController")
@ViewScoped
public class ForgotPasswordJsfController extends AbstractJsfController {

	private static final long serialVersionUID = 1L;

	private static final Logger logger = LoggerFactory.getLogger(ForgotPasswordJsfController.class);

	public void doSendPassword() {
		String email = text.get("email").trim();
		logger.debug("'"+ email + "' requesting to send password");

		EntityManager em = PersistenceManager.getInstance().getEntityManager();
		try {
			FormValidationUtil.validateEmail(email);
			
			Customer customer = new CustomerDAO(em).findByEmail(email);
			if (customer == null) {
				throw new EntityNotFoundException("Email '" + email + "' does not exist");
			}
			sendPassword(em, customer);
			addInfoMessage("Please check your email for your password");
		} catch (FormValidationException | EntityNotFoundException e) {
			addErrorMessage(e.getMessage());
		} catch (Exception e) {
			addErrorMessage(e.getMessage());
		} finally {
			PersistenceManager.closeEntityManager(em);
		}
	}

	private void sendPassword(EntityManager em, Customer customer) {
		NotificationService notificationService = new NotificationService();
		if (StringUtils.isEmpty(customer.getPasswordText())) {
			new CustomerService().updatePassword(em, customer);
			notificationService.notifyCustomerPassword(customer.getEmail(), customer.getPasswordText());
		} else {
			notificationService.notifyCustomerPassword(customer.getEmail(), customer.getPasswordText());
		}
	}
}
