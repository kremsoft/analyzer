package com.ardor.controller.jsf;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import javax.faces.bean.ManagedBean;
import javax.persistence.EntityManager;

import org.jsoup.helper.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ardor.entity.Customer;
import com.ardor.entity.Website;
import com.ardor.exception.FormValidationException;
import com.ardor.persistence.PersistenceManager;
import com.ardor.service.CustomerService;
import com.ardor.service.CustomerWebsiteService;
import com.ardor.service.WebsiteService;
import com.ardor.service.scheduler.SchedulerService;
import com.ardor.util.DownloadUtil;
import com.ardor.util.FormValidationUtil;
import com.ardor.util.PageUtil;

@ManagedBean(name = "registerCustomerJsfController")
public class RegisterCustomerJsfController extends AbstractJsfController {

	private static final long serialVersionUID = 1L;

	private static final Logger logger = LoggerFactory.getLogger(RegisterCustomerJsfController.class);

	private int depth = 0;

	private List<String> numberOfDepths;

	public RegisterCustomerJsfController() {
		long t1 = System.currentTimeMillis();
		
		numberOfDepths = new ArrayList<>(
				Arrays.asList("0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15"));
		
		depth = Integer.parseInt(numberOfDepths.get(0));
		
		long t2 = System.currentTimeMillis();
		logger.debug("elapsed time = {}s", (t2 - t1) / 1000.0);
	}

	public void doRegister() {
		String url = text.get("url").trim();
		String email = text.get("email").trim();
		String customDepth = text.get("customDepth").trim();

		if ( ! customDepth.isEmpty() && StringUtil.isNumeric(customDepth)) {
			depth = Integer.parseInt(customDepth);
		}

		EntityManager em = PersistenceManager.getInstance().getEntityManager();
		try {
			FormValidationUtil.validateEmail(email);
			FormValidationUtil.validateUrl(url);
			
			String urlWithSlashAtEnd = url.endsWith("/") ? url : url + "/";

			boolean escapeFragment = false;
			String htmlPage = DownloadUtil.downloadPage(url, escapeFragment);
			
			WebsiteService websiteService = new WebsiteService();
			if (websiteService.isUrlNotExist(em, url) && websiteService.isUrlNotExist(em, urlWithSlashAtEnd) && websiteService.isContentNotExist(em, htmlPage)) {
				CustomerService customerService = new CustomerService();
				String generatedPassword = customerService.generateRandomPassword();
				Customer customer = customerService.createCustomer(em, email, generatedPassword);
				
				String generateduuID = UUID.randomUUID().toString();
				scheduleCrawl(em, htmlPage, url, urlWithSlashAtEnd, customer, generateduuID, generatedPassword);
				
				logger.info("doCrawl() {}", url);
				redirectInternal("/registerSuccess.xhtml");
			} else {
				addErrorMessage("URL already crawled");
			}
		} catch(FormValidationException e) { 
			addErrorMessage(e.getMessage());
		} catch (Throwable e) {
			addErrorMessage(e.getMessage());
			logger.error(e.getMessage(), e);
		} finally {
			PersistenceManager.closeEntityManager(em);
		}
	}

	public void onDepthChange() {
		depth = Integer.parseInt(numberOfDepths.get(depth));
		logger.info("onDepthChange() Selected number of depth is {}", depth);
	}

	private void scheduleCrawl(EntityManager em, String htmlPage, String url, String urlWithSlashAtEnd,
			Customer customer, String generateduuID, String generatedPassword) {
		
		List<String> allLinks = PageUtil.collectLinks(htmlPage);
		List<String> internalLinks = PageUtil.filterByInternalLinks(url, allLinks);

		int totalInternalLinks = internalLinks.size();
		int totalExternalLinks = allLinks.size() - internalLinks.size();

		new CustomerWebsiteService().createCustomerWebsite(em, customer, generateduuID, depth);
		new WebsiteService().create(em, 0L, urlWithSlashAtEnd, htmlPage, generateduuID, depth, totalInternalLinks, totalExternalLinks);
		
		SchedulerService schedulerService = new SchedulerService();
		schedulerService.schedulePersistSiteHierarchyJob(depth, Website.PARENT_DEFAULT_ID, url, generateduuID, 0);
		schedulerService.scheduleCrawlingDoneChecker(generateduuID);
	}

	public int getDepth() {
		return depth;
	}

	public void setDepth(int depth) {
		this.depth = depth;
	}

	public List<String> getNumberOfDepths() {
		return numberOfDepths;
	}

	public void setNumberOfDepths(List<String> numberOfDepths) {
		this.numberOfDepths = numberOfDepths;
	}
}
