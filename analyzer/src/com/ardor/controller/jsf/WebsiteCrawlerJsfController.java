package com.ardor.controller.jsf;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.persistence.EntityManager;

import org.jsoup.helper.StringUtil;
import org.primefaces.component.datatable.DataTable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ardor.dao.CustomerWebsiteDAO;
import com.ardor.entity.Customer;
import com.ardor.entity.Website;
import com.ardor.exception.FormValidationException;
import com.ardor.model.WebsiteLazyDataModel;
import com.ardor.persistence.PersistenceManager;
import com.ardor.service.CustomerWebsiteService;
import com.ardor.service.WebsiteService;
import com.ardor.service.scheduler.SchedulerService;
import com.ardor.util.DownloadUtil;
import com.ardor.util.FormValidationUtil;
import com.ardor.util.PageUtil;

@ManagedBean(name = "websiteCrawlerJsfController")
@ViewScoped
public class WebsiteCrawlerJsfController extends AbstractJsfController {

	private static final long serialVersionUID = 1L;

	private static final Logger logger = LoggerFactory.getLogger(WebsiteCrawlerJsfController.class);

	private WebsiteLazyDataModel websiteLazyDataModel;

	private List<String> numberOfDepths = new ArrayList<>(
			Arrays.asList("0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15"));;

	private int depth = 0;

	public WebsiteCrawlerJsfController() {
		long t1 = System.currentTimeMillis();
		
		EntityManager em = PersistenceManager.getInstance().getEntityManager();
		try {
			Customer customer = getCurrentCustomer();
			List<String> uuIDs = new CustomerWebsiteDAO(em).findUUIDs(customer.getId());
			websiteLazyDataModel = new WebsiteLazyDataModel(uuIDs);
		} catch (Exception e) { 
			logger.warn(e.getMessage(), e);
		} finally {
			PersistenceManager.closeEntityManager(em);
		}
		
		depth = Integer.parseInt(numberOfDepths.get(0));
		
		long t2 = System.currentTimeMillis();
		logger.debug("elapsed time = {}s", (t2 - t1) / 1000.0);
	}

	public void doCrawl() {
		String url = text.get("url").trim();
		String customDepth = text.get("customDepth").trim();

		if ( ! customDepth.isEmpty() && StringUtil.isNumeric(customDepth)) {
			depth = Integer.parseInt(customDepth);
		}

		EntityManager em = PersistenceManager.getInstance().getEntityManager();
		try {
			FormValidationUtil.validateUrl(url);
			String urlWithSlashAtEnd = url.endsWith("/") ? url : url + "/";

			boolean escapeFragment = false;
			String htmlPage = DownloadUtil.downloadPage(url, escapeFragment);

			WebsiteService websiteService = new WebsiteService();
			if (websiteService.isUrlNotExist(em, url) && websiteService.isUrlNotExist(em, urlWithSlashAtEnd) && websiteService.isContentNotExist(em, htmlPage)) {
				Customer customer = getCurrentCustomer();
				String generateduuID = UUID.randomUUID().toString();
				scheduleCrawl(em, htmlPage, url, urlWithSlashAtEnd, customer, generateduuID);

				logger.info("doCrawl() {}", url);
				addInfoMessage("Will send you an email, we are still crawling your website.");
			} else {
				addErrorMessage("URL already crawled");
			}
		} catch (FormValidationException e) {
			logger.warn(e.getMessage(), e);
			addErrorMessage(e.getMessage());
		} catch (Throwable e) {
			addErrorMessage("Something goes wrong");
			logger.warn(e.getMessage(), e);
		} finally {
			PersistenceManager.closeEntityManager(em);
		}

		DataTable dataTable = (DataTable) getComponent(":form_1:dataTable_1");
		dataTable.loadLazyData();
	}

	public void doDelete(String uuID) {
		EntityManager em = PersistenceManager.getInstance().getEntityManager();
		try {
			boolean isSiteDeleted = new WebsiteService().deleteAllByUUID(em, uuID);
			boolean isUserDeleted = new CustomerWebsiteService().deleteCustomerWebsite(em, uuID);
			if (isSiteDeleted && isUserDeleted) {
				addInfoMessage("Successfully Deleted");
			}
		} catch (Throwable e) {
			addErrorMessage("Failed to delete record");
			logger.warn("doDelete() {}", e);
		} finally {
			PersistenceManager.closeEntityManager(em);
		}
	}

	public void onDepthChange() {
		depth = Integer.parseInt(numberOfDepths.get(depth));
		logger.info("onDepthChange() Selected number of depth is {}", depth);
	}

	private void scheduleCrawl(EntityManager em, String htmlPage, String url, String urlWithSlashAtEnd, Customer customer, String generateduuID) {
		List<String> allLinks = PageUtil.collectLinks(htmlPage);
		List<String> internalLinks = PageUtil.filterByInternalLinks(url, allLinks);

		int totalInternalLinks = internalLinks.size();
		int totalExternalLinks = allLinks.size() - internalLinks.size();

		new CustomerWebsiteService().createCustomerWebsite(em, customer, generateduuID, depth);
		new WebsiteService().create(em, 0L, urlWithSlashAtEnd, htmlPage, generateduuID, depth, totalInternalLinks, totalExternalLinks);
		
		SchedulerService schedulerService = new SchedulerService();
		schedulerService.schedulePersistSiteHierarchyJob(depth, Website.PARENT_DEFAULT_ID, url, generateduuID, 0);
		schedulerService.scheduleCrawlingDoneChecker(generateduuID);
	}

	public boolean isDoneCrawling(String uuID) {
		boolean isDoneCrawling = false;
		EntityManager em = PersistenceManager.getInstance().getEntityManager();
		try {
			isDoneCrawling = new CustomerWebsiteDAO(em).findByUUID(uuID).isDoneCrawling();
		} catch (Throwable e) {
			logger.warn("isProcessing() {}", e);
		} finally {
			PersistenceManager.closeEntityManager(em);
		}
		return isDoneCrawling;
	}
	
	public void doLogout() {
		logger.debug("doLogout()");
		getSession().invalidate();
		redirectInternal("/customer-login");
	}

	public WebsiteLazyDataModel getWebsiteLazyDataModel() {
		return websiteLazyDataModel;
	}

	public void setWebsiteLazyDataModel(WebsiteLazyDataModel websiteLazyDataModel) {
		this.websiteLazyDataModel = websiteLazyDataModel;
	}

	public int getDepth() {
		return depth;
	}

	public void setDepth(int depth) {
		this.depth = depth;
	}

	public List<String> getNumberOfDepths() {
		return numberOfDepths;
	}

	public void setNumberOfDepths(List<String> numberOfDepths) {
		this.numberOfDepths = numberOfDepths;
	}
}
