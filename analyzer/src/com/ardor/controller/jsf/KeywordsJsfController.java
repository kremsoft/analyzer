package com.ardor.controller.jsf;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ardor.dao.WebsiteDAO;
import com.ardor.entity.Website;
import com.ardor.model.data.ResultModel;
import com.ardor.persistence.PersistenceManager;
import com.ardor.util.PageUtil;
import com.ardor.util.StringUtil;

@ManagedBean(name = "keywordsJsfController")
@ViewScoped
public class KeywordsJsfController extends AbstractJsfController {

	private static final long serialVersionUID = 1L;

	private static final Logger logger = LoggerFactory.getLogger(KeywordsJsfController.class);

	private Map<String, String> paramMap;
	private String[] keywords;
	private List<ResultModel> results;

	private boolean showForm_1;
	private boolean showForm_2;
	
	private String uuID = StringUtil.EMPTY_STRING;
	
	public KeywordsJsfController() {
		long t1 = System.currentTimeMillis();
		keywords = new String[8];
		results = new ArrayList<ResultModel>();
		paramMap = getParameterMap();
		showForm_1 = true;
		showForm_2 = false;
		EntityManager em = PersistenceManager.getInstance().getEntityManager();
		try {
			long websiteId = StringUtil.stringToLong(paramMap.get("websiteId"), 0L);
			Website website = new WebsiteDAO(em).findById(websiteId);
			if (website != null) {
				text.put("url", website.getUrl());
				uuID = website.getUuID();
			}
		} finally {
			PersistenceManager.closeEntityManager(em);
		}
		long t2 = System.currentTimeMillis();
		logger.debug("elapsed time = {}s", (t2 - t1) / 1000.0);
	}

	public void doAnalyze() {
		EntityManager em = PersistenceManager.getInstance().getEntityManager();
		WebsiteDAO websiteDAO = new WebsiteDAO(em);
		try {
			results.clear();
			for (String keyword : keywords) {
				logger.info("-->{}", keyword);
				if (!keyword.trim().isEmpty()) {
					List<Website> websites = websiteDAO.findByKeyword(keyword, uuID);
					List<Website> filteredSites = PageUtil.filterKeyword(keyword, websites);
					ResultModel resultDTO = new ResultModel();
					resultDTO.setKeyword(keyword);
					resultDTO.setWebsites(filteredSites);
					results.add(resultDTO);
				}
			}
		} catch (Exception e) {
			addErrorMessage(e.getMessage());
		} finally {
			PersistenceManager.closeEntityManager(em);
		}
		addInfoMessage("Analysis Complete!");
		showForm_2 = true;
	}

	public void doClear() {
		for (int i = 0; i < keywords.length; i++) {
			keywords[i] = StringUtil.EMPTY_STRING;
		}
		showForm_2 = false;
	}
	
	public void doLogout() {
		logger.warn("doLogout()");
		getSession().invalidate();
		redirectInternal("");
	}

	public String[] getKeywords() {
		return keywords;
	}

	public void setKeywords(String[] keywords) {
		this.keywords = keywords;
	}

	public List<ResultModel> getResults() {
		return results;
	}

	public void setResults(List<ResultModel> results) {
		this.results = results;
	}

	public boolean isShowForm_1() {
		return showForm_1;
	}

	public void setShowForm_1(boolean showForm_1) {
		this.showForm_1 = showForm_1;
	}

	public boolean isShowForm_2() {
		return showForm_2;
	}

	public void setShowForm_2(boolean showForm_2) {
		this.showForm_2 = showForm_2;
	}
}
