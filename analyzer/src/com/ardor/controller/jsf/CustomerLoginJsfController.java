package com.ardor.controller.jsf;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.persistence.EntityManager;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ardor.entity.Customer;
import com.ardor.persistence.PersistenceManager;
import com.ardor.service.LoginService;
import com.ardor.util.SessionAttributes;
import com.ardor.util.StringUtil;

@ManagedBean(name = "customerLoginJsfController")
@ViewScoped
public class CustomerLoginJsfController extends AbstractJsfController{

	private static final long serialVersionUID = 1L;
	
	private static final Logger logger = LoggerFactory.getLogger(CustomerLoginJsfController.class);
	
	private String fromUrl;
	
	public CustomerLoginJsfController() {
		fromUrl = StringUtil.EMPTY_STRING;
		if (getRequest().getParameter("from") != null) {
			fromUrl = getRequest().getParameter("from");
			logger.debug("fromUrl " + fromUrl );
		}
	}
	
	public void doLogin() {
		logger.debug("doLogIn()");
		String email = text.get("email").trim();
		String password = text.get("password").trim();
		
		EntityManager em = PersistenceManager.getInstance().getEntityManager();
		try {
			Customer customer = new LoginService().authenticate(em, email, password);
			
			HttpSession session = getSession();
			session.setMaxInactiveInterval(3600); // session-timeout to 60mins
			session.setAttribute(SessionAttributes.SESSION_CUSTOMER, customer);
			session.setAttribute(SessionAttributes.SESSION_USER_AUTHENTICATION_MODE, "internal");
			
			logger.debug("fromUrl doLogin " + fromUrl );
			if ( ! fromUrl.isEmpty()) {
				logger.debug("else fromUrl " + fromUrl );
				redirectInternal(fromUrl);
				return;
			}
			
			redirectInternal("/website-crawler");
		} catch (Exception e) {
			addErrorMessage(e.getMessage());
			//logger.warn("doLogin() email = '{}' ", email, e);
			
		} finally {
			PersistenceManager.closeEntityManager(em);
		}
	}
}
