package com.ardor.controller.jsf;

import java.io.IOException;
import java.io.Serializable;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ardor.entity.Customer;
import com.ardor.util.SessionAttributes;
import com.ardor.util.SessionAttributes.UserType;
import com.ardor.util.StringUtil;

/**
 * @author rmagallen
 */
public abstract class AbstractJsfController implements Serializable {

	private static final long serialVersionUID = 1L;
	private static final Logger logger = LoggerFactory.getLogger(AbstractJsfController.class);

	protected Map<String, String> text;
	protected String viewId;

	public AbstractJsfController() {
		this.text = new HashMap<String, String>();
	}

	protected HttpSession getSession() {
		FacesContext fc = FacesContext.getCurrentInstance();
		ExternalContext ec = fc.getExternalContext();
		return (HttpSession) ec.getSession(true);
	}

	protected HttpServletRequest getRequest() {
		FacesContext fc = FacesContext.getCurrentInstance();
		ExternalContext ec = fc.getExternalContext();
		return (HttpServletRequest) ec.getRequest();
	}

	protected HttpServletResponse getResponse() {
		FacesContext fc = FacesContext.getCurrentInstance();
		ExternalContext ec = fc.getExternalContext();
		return (HttpServletResponse) ec.getResponse();
	}

	protected Map<String, String> getParameterMap() {
		FacesContext fc = FacesContext.getCurrentInstance();
		ExternalContext ec = fc.getExternalContext();
		return ec.getRequestParameterMap();
	}

	protected Cookie getCookie(String name) {
		if (getRequest().getCookies() != null) {
			Cookie[] cookieList = getRequest().getCookies();
			for (Cookie cookie : cookieList) {
				if (cookie.getName().equals(name)) {
					return cookie;
				}
			}
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	protected <T> T getManagedBean(String name) {
		FacesContext fc = FacesContext.getCurrentInstance();
		return (T) fc.getApplication().evaluateExpressionGet(fc, "#{" + name + "}", Object.class);
	}

	protected UIComponent getComponent(String componentId) {
		FacesContext fc = FacesContext.getCurrentInstance();
		return fc.getViewRoot().findComponent(componentId);
	}

	protected void addInfoMessage(String message) {
		FacesMessage.Severity s = FacesMessage.SEVERITY_INFO;
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(s, StringUtil.EMPTY_STRING, message));
	}

	protected void addWarnMessage(String message) {
		FacesMessage.Severity s = FacesMessage.SEVERITY_WARN;
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(s, StringUtil.EMPTY_STRING, message));
	}

	protected void addErrorMessage(String message) {
		FacesMessage.Severity s = FacesMessage.SEVERITY_ERROR;
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(s, StringUtil.EMPTY_STRING, message));
	}

	protected void addFatalMessage(String message) {
		FacesMessage.Severity s = FacesMessage.SEVERITY_FATAL;
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(s, StringUtil.EMPTY_STRING, message));
	}

	protected void redirectExternal(String url) {
		try {
			FacesContext fc = FacesContext.getCurrentInstance();
			ExternalContext ec = fc.getExternalContext();
			ec.redirect(url);
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
	}

	protected void redirectInternal(String uri) {
		try {
			FacesContext fc = FacesContext.getCurrentInstance();
			ExternalContext ec = fc.getExternalContext();
			ec.redirect(ec.getRequestContextPath() + uri);
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
	}

	protected void sendErrorResponse(int errorCode) {
		FacesContext fc = FacesContext.getCurrentInstance();
		ExternalContext ec = fc.getExternalContext();
		try {
			((HttpServletResponse) ec.getResponse()).sendError(errorCode);
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
	}

	protected String svo(Serializable s) {
		return String.valueOf(s);
	}

	protected void logRequest(HttpServletRequest request) {
		logger.info("---------------------------------------------------");
		logger.info("logRequest() {}", new java.util.Date());
		logger.info(request.getRequestURL().toString());
		Enumeration<String> enu = request.getParameterNames();
		while (enu.hasMoreElements()) {
			String name = enu.nextElement();
			String value = request.getParameter(name);
			logger.info("{}={}", name, value);
		}
		logger.info("---------------------------------------------------");
	}

	protected String getRemoteIpAddr() {
		String forwardedFor = getRequest().getHeader("X-Forwarded-For");
		if (forwardedFor != null && !forwardedFor.isEmpty()) {
			return forwardedFor.split("\\s*,\\s*", 2)[0];
		}
		return getRequest().getRemoteAddr();
	}

	public Map<String, String> getText() {
		return text;
	}

	public void setText(Map<String, String> text) {
		this.text = text;
	}

	public String getViewId() {
		return viewId;
	}

	public void setViewId(String viewId) {
		this.viewId = viewId;
	}
	
	protected UserType getUserType() {
		return (UserType) getRequest().getAttribute(SessionAttributes.SESSION_USER_TYPE);
	}
	
	protected Customer getCurrentCustomer() {
		return (Customer) getSession().getAttribute(SessionAttributes.SESSION_CUSTOMER);
	}
}
