package com.ardor.servlet;

import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DownloadLog extends HttpServlet {
	
	private static final Logger logger = LoggerFactory.getLogger(DownloadLog.class);
			
	private static final long serialVersionUID = 1L;

	public void doGet(HttpServletRequest request, HttpServletResponse response) {
		String filePath = System.getProperty("catalina.base") + File.separator + "logs" + File.separator;
		File logFile = new File(filePath + "analyzer.log");
		
		if (logFile != null && logFile.exists()) {
			response.setHeader("Content-Disposition", "attachment;filename=" + logFile.getName() + ".txt");
			response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1
			response.setHeader("Pragma", "no-cache"); // HTTP 1.0
			response.setDateHeader("Expires", 0); // Proxies
			response.setContentType("application/octet-stream");
			
			OutputStream out = null;
			try {
				out = response.getOutputStream();
				byte[] fileContent = IOUtils.toByteArray(new FileInputStream(logFile));
				out.write(fileContent);
			} catch (Exception e) {
				logger.warn("DownloadLog() {}", e);
			} finally {
				try {
					out.close();
				} catch (Exception e) {
					logger.warn("out.close() {}", e);
				}
			}
		}
	}
}
