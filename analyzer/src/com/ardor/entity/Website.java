package com.ardor.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

@Entity
@Table(name = "website")
public class Website implements Serializable {

	private static final long serialVersionUID = 1L;
	
	public static final long PARENT_DEFAULT_ID = 0L;

	@Id
	@GeneratedValue
	private long id;

	private long parentId;

	private String url;

	@Lob
	@Basic(fetch = FetchType.LAZY)
	@Column(length = 100000)
	private String content;
	
	private String uuID;
	
	private int depthCount;
	
	private int totalInternalLinks;
	
	private int totalExternalLinks;
	
	private long contentCode;
	
	private Date dateCrawled;
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getParentId() {
		return parentId;
	}

	public void setParentId(long parentId) {
		this.parentId = parentId;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getUuID() {
		return uuID;
	}

	public void setUuID(String uuID) {
		this.uuID = uuID;
	}

	public int getDepthCount() {
		return depthCount;
	}

	public void setDepthCount(int depthCount) {
		this.depthCount = depthCount;
	}
	
	public int getTotalInternalLinks() {
		return totalInternalLinks;
	}

	public void setTotalInternalLinks(int totalInternalLinks) {
		this.totalInternalLinks = totalInternalLinks;
	}
	
	public int getTotalExternalLinks() {
		return totalExternalLinks;
	}

	public void setTotalExternalLinks(int totalExternalLinks) {
		this.totalExternalLinks = totalExternalLinks;
	}

	public long getContentCode() {
		return contentCode;
	}

	public void setContentCode(long contentCode) {
		this.contentCode = contentCode;
	}
	
	public Date getDateCrawled() {
		return dateCrawled;
	}

	public void setDateCrawled(Date dateCrawled) {
		this.dateCrawled = dateCrawled;
	}

	@Override
	public boolean equals(Object object) {
		if (object == null) {
			return false;
		}
		if (object.getClass() != getClass()) {
			return false;
		}
		Website website = (Website) object;
		return new EqualsBuilder().append(id, website.getId()).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 31).append(id).toHashCode();
	}

	@Override
	public String toString() {
		return "Website [ id = " + id + ", parentId = " + parentId + ", url = " + url + " ]";
	}
}
