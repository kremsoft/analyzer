package com.ardor.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

@Entity
@Table(name = "customer_website")
public class CustomerWebsite implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	private long id;

	@ManyToOne
	@JoinColumn(name = "customerID")
	private Customer customer;

	private String siteUUID;

	@Column(columnDefinition = "bit")
	private boolean isDoneCrawling;

	private int selectedDepth;

	public long getId() {
		return id;
	}
	
	public String getSiteUUID() {
		return siteUUID;
	}

	public void setSiteUUID(String siteUUID) {
		this.siteUUID = siteUUID;
	}

	public boolean isDoneCrawling() {
		return isDoneCrawling;
	}

	public void setDoneCrawling(boolean isDoneCrawling) {
		this.isDoneCrawling = isDoneCrawling;
	}

	public int getSelectedDepth() {
		return selectedDepth;
	}

	public void setSelectedDepth(int selectedDepth) {
		this.selectedDepth = selectedDepth;
	}
	
	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	@Override
	public boolean equals(Object object) {
		if (object == null) {
			return false;
		}
		if (object.getClass() != getClass()) {
			return false;
		}
		CustomerWebsite user = (CustomerWebsite) object;
		return new EqualsBuilder().append(id, user.getId()).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 31).append(id).toHashCode();
	}

	@Override
	public String toString() {
		return "User [ id = " + id + ", customerID = " + customer.getId() + " ]";
	}

}
