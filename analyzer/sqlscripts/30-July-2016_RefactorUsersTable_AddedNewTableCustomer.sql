TRUNCATE TABLE `mobwar_analyzer`.`user`;
TRUNCATE TABLE `mobwar_analyzer`.`website`;

ALTER TABLE `mobwar_analyzer`.`user`   
  CHANGE `email` `customerID` BIGINT(20) NULL;

RENAME TABLE `mobwar_analyzer`.`user` TO `mobwar_analyzer`.`customer_website`;

CREATE TABLE `customer` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `passwordSalt` varchar(255) DEFAULT NULL,
  `dateCreated` datetime DEFAULT NULL,
  `passwordText` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `EMAIL` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

